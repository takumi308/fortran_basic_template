program main
        implicit none
        real(4)::S_sq,S_ci,x,y,r,pi,N_double
        integer(4)::i,inner,N

        S_sq=1.0
        r=1.0
        N=1000
        x=0
        y=0
        N_double=0

        do i=1,N
                call random_number(x)
                call random_number(y)
                if(x*x+y*y<1)then
                        inner=inner+1
                endif
                N_double=N_double+1
        enddo
        S_ci=inner/N_double
        pi=4*S_ci/(r*r)
        write(*,*)pi

end program main

